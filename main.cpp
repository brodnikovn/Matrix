#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cmath>
#include <fstream>
#include <fcntl.h>
using namespace std;
struct matrix
{
	int** A;
	size_t n;
	size_t m;
};
void  downloading(matrix);
void rec(matrix);
void reverse(matrix a);
int det_matrix_rec(int **a, int m);
void algebraic_complements(int **, int **, int, int, int);
matrix case_4(matrix a);
void det(matrix a);
void modification(matrix a);
matrix polynomial(matrix a);
matrix case_5(matrix q);
matrix memory_allocation(matrix);
void memory_cleaning(matrix);
matrix power(matrix a);
void conklusion_matrix(matrix a);
void search(matrix a, int k);
matrix fill_matrix(matrix a);
void case_2(matrix A);
void case_15(matrix a);
void case_3(matrix k);
matrix case_4(matrix a);
int main(int argc, char* argv[]) {
	char *locale = setlocale(LC_ALL, "RUS");
	matrix a;
	bool empty;

	if (argc != 1) {
		a.n = atoi(argv[1]);
		a.m = atoi(strchr(argv[1], 'x') + 1);
	}
	if (argc == 1) {
		a.n = 0;
		a.m = 0;
	}
	a = memory_allocation(a);
	if (argc == 1) {
		cout << "вы ничего не ввели" << endl;
		empty = true;
	}
	if (argc == 2) {
		empty = true;
	}
	if (argc >= 3) {
		if (argc == 3) {
			char* open = argv[2];
			char* end = strchr(argv[2], '\0') - 1;
			for (int i = 0; i < a.n; i++) {
				for (int j = 0; j < a.m; j++) {
					a.A[i][j] = atoi(open);
					if (open == end) break;
					open = strchr(open, ',') + 1;
				}
				if (open == end) break;
			}
		}
		else {
			int k = 2;
			for (int i = 0; i < a.n; i++) {
				for (int j = 0; j < a.m && k < argc; j++, k++) {
					a.A[i][j] = atoi(argv[k]);
				}
			}
		}
		empty = false;
	}

	for (;;) {

		cout << "выберите :" << endl
			<< "1. Вывести матрицу" << endl
			<< "2. Сложить матрицу" << endl
			<< "3. Умножить матрицу" << endl
			<< "4. Транспонировать матрицу" << endl
			<< "5. Расширить матрицу" << endl
			<< "6. Найти элементы" << endl
			<< "7. Изменить значение элемента" << endl
			<< "8. Возвести в степень" << endl
			<< "9. Вычислить определитель матрицы" << endl
			<< "10. Вычислить обратную матрицу" << endl
			<< "11. Вычислить многочлен матрицы" << endl
			<< "12. Сохранить в файл" << endl
			<< "13. Загрузить из файла" << endl
			<< "14. Сортировать матрицу" << endl
			<< "15. Выйти из программы" << endl;
		int N;
		cin >> N;

		switch (N) {
		case 1: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
			}
			else {
				cout << "Результат: " << endl;
				conklusion_matrix(a);
			}
			break;
		}
		case 2: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
			}
			else {
				cout << "введите матрицу  " << a.n << "x" << a.m << endl;
				case_2(a);
				break;
			}
			break;
		}
		case 3: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
			}
			else {
				cout << "Введите размер матрицы:" << endl;
				case_3(a);
			}
			break;
		}
		case 4: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
			}
			else {
				a = case_4(a);
			}
			break;
		}
		case 5: {

			a = case_5(a);
			empty = false;
			break;
		}
		case 6: {
			if (empty == true) {
				cout << "матрица пустая!\n" << endl;
			}
			else {
				cout << "Введите значение :" << endl;
				int zn;
				cin >> zn;
				search(a, zn);
				cout << endl;
			}
			break;

		}
		case 7: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
			}
			else {
				modification(a);
			}
			break;
		}
		case 8: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
			}
			else {
				cout << "Введите степень :" << endl;
				a = power(a);
			}
			break;
		}
		case 9: {
			if (empty == true || a.n != a.m) {
				cout << "не возможно вычислить определитель" << endl;
			}
			else {
				det(a);
			};
			break;
		}
		case 10: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
				break;
			}
			else {
				if (a.n != a.m) {
					cout << "матрица не квадратная " << endl;
				}
				else {
					reverse(a);
				}

				break;
			}
		}
		case 11: {
			if (empty == true) {
				cout << "Матрица пуста" << endl;
			}
			else {
				a = polynomial(a);
			}
			break;
		}
		case 12: {
			rec(a);
			break;
		}
		case 13: {
			downloading(a);
			break;
		}
		case 14: {
			if (empty) {
				cout << "матрица пустая!\n";
			}
			else {
				cout << "выберите порядок сортировки:\n s змейкой \n e спиралью \n a по строкам" << endl;
				char vid;
				cin >> vid;
				int sp;
				int num ;
				int rw;
				int cl ;
				int tek = 0, size = a.n* a.m, temp;
				int* s = new int[size];
				for (int i = 0; i < a.n; i++) {
					for (int j = 0; j < a.m; j++) {
						s[tek] = a.A[i][j];
						tek++;
					}
				}
				for (int i = 0; i < size - 1; i++) {
					for (int j = 0; j < size - i - 1; j++) {
						if (s[j] > s[j + 1]) {
							temp = s[j];
							s[j] = s[j + 1];
							s[j + 1] = temp;
						}
					}
				}
				switch (vid) {
				case 's':
					for (int i = 0; i < a.n; i++) {
						for (int j = 0; j < a.m; j++) {
							if (j % 2 == 0) {
								a.A[i][j] = s[a.m * j + i];
							}
							else {
								a.A[i][j] = s[a.n * (j + 1) - i - 1];
							}
						}
					}
					break;
				case 'e':
					 sp = 0;
					 num = 0;
					 rw = 0;
					 cl = -1;
					while (num < size) {
						while ((num < size) && (cl + 1 < a.m - sp)) {
							cl++;
							a.A[rw][cl] = s[num];
							num++;
						}
						while ((num < size) && (rw + 1 < a.n - sp)) {
							rw++;
							a.A[rw][cl] = s[num];
							num++;
						}
						while ((num < size) && (cl - 1 >= sp)) {
							cl--;
							a.A[rw][cl] = s[num];
							num++;
						}
						sp++;
						while ((num < size) && (rw - 1 >= sp)) {
							rw--;
							a.A[rw][cl] = s[num];
							num++;
						}
					}
					break;
				case 'a':
					num = 0;
					for (int i = 0; i < a.n; i++) {
						for (int j = 0; j < a.m; j++) {
							a.A[i][j] = s[num];
							num++;
						}
					}
					break;
				default:
					cout << "неприемлемый символ\n";
				}

				delete[] s;
			}
			break;

		}
		case 15: {
			case_15(a);
			break;
		}

		default: {
			cout << " команда не распознана " << endl;
			cin.clear();
			cin.get();
			break;
		}
		}

	}
	memory_cleaning(a);
	return 0;
}

matrix memory_allocation(matrix a) {
	matrix b;
	b.A = new int*[a.n];
	for (int i = 0; i < a.n; i++) {
		b.A[i] = new int[a.m];
	}
	for (int i = 0; i < a.n; i++) {
		for (int j = 0; j < a.m; j++) {
			b.A[i][j] = 0;
		}
	}
	b.n = a.n;
	b.m = a.m;
	return b;
	memory_cleaning(b);
}


void memory_cleaning(matrix A) {
	for (int i = 0; i < A.n; i++)
		delete[] A.A[i];
	delete[] A.A;
}

void conklusion_matrix(matrix a) {

	for (int i = 0; i < a.n; ++i) {
		for (int j = 0; j < a.m; ++j) {
			cout << a.A[i][j] << "  ";

		}
		cout << endl;
	}
}

matrix fill_matrix(matrix a) {
	matrix k = memory_allocation(a);
	for (int i = 0; i < a.n; i++)
		for (int j = 0; j < a.m; j++) {
			cin >> k.A[i][j];
			while (!cin) {
				cout << "ошибка, введите число ,а не букву!!\n";
				cin.clear();
				cin.get();
				cin >> k.A[i][j];
			}
		}
	return k;
}

void case_2(matrix A) {
	matrix k;
	k.n = A.n;
	k.m = A.m;
	k = fill_matrix(k);
	for (int i = 0; i < A.n; i++)
		for (int j = 0; j < A.m; j++)
			k.A[i][j] += A.A[i][j];
	cout << "Результат:" << endl;
	conklusion_matrix(k);
	memory_cleaning(k);
}

void case_15(matrix a) {
	cout << "Вы хотите выйти из программы ? (y/N):" << endl;
	string answer;
	cin >> answer;
	if ((answer == "y") || (answer == "Y") || (answer == "yes") || (answer == "Yes") || (answer == "YES")) {
		memory_cleaning(a);
		exit(0);
	}
	if ((answer == "n") || (answer == "no") || (answer == "NO") || (answer == "No") || (answer == "N")) {
		cout << " отмена операции " << endl;
	}
	else {
		cout << " команда не распознана " << endl;
	}
	cin.clear();
}

void case_3(matrix k) {
	char v[4];
	int n1 = 0, m1 = 0;
	cin >> v;
	n1 = atoi(v);
	m1 = atoi(strchr(v, 'x') + 1);
	if ((n1<0) || (m1<0)) {
		return;
	}
	if (n1*m1 == 0) {
		cout << "умножение не может быть произведено (введена пустая матрица )" << endl;
		return;
	}
	if (k.m != n1) {
		cout << "неверный размер" << endl;
		return;
	}
	else {
		cout << "Введите элементы матрицы:" << endl;
		matrix a;
		a.n = n1;
		a.m = m1;
		a = fill_matrix(a);
		cout << "Результат" << endl;
		int s;
		for (int i = 0; i<k.n; i++) {
			for (int l = 0; l<m1; l++) {
				s = 0;
				for (int j = 0; j<a.m; j++) {
					s += k.A[i][j] * a.A[j][l];
				}
				cout << s << "   ";
			}

			cout << endl;
		}
		memory_cleaning(a);
	}
}

matrix case_4(matrix a) {
	matrix tra;
	tra.A = new int*[a.m];
	for (int i = 0; i < a.m; i++) {
		tra.A[i] = new int[a.n];
	}
	for (int i = 0; i < a.m; i++) {
		for (int j = 0; j < a.n; j++) {
			tra.A[i][j] = a.A[j][i];
		}
	}
	tra.n = a.m;
	tra.m = a.n;
	memory_cleaning(a);
	return tra;

}

matrix case_5(matrix q) {
	char a;
	cout << " h <\n k ^\n l >\n j v\n";
	cin >> a;
	while (!cin) {
		cout << "ошибка, некоректный ввод!!\n";
		cin.clear();
		cin.get();
		return q;
	}
	switch (a) {
	case'h': {
		if ((q.n == 0) || (q.m == 0)) {
			matrix Z;
			Z.n = Z.m = 1;
			Z = memory_allocation(Z);
			memory_cleaning(q);
			return Z;
		}
		else {
			matrix Z; Z.n = q.n; Z.m = q.m + 1;
			Z = memory_allocation(Z);
			for (int i = 0; i<q.n; i++)
				for (int j = 0; j<q.m; j++) {
					Z.A[i][j + 1] = q.A[i][j];
				}
			memory_cleaning(q);
			return Z;
		}
	}
	case'k': {
		if ((q.n == 0) || (q.m == 0)) {
			matrix Z;
			Z.n = Z.m = 1;
			Z = memory_allocation(Z);
			memory_cleaning(q);
			return Z;
		}
		else {
			matrix Z; Z.n = q.n + 1; Z.m = q.m;
			Z = memory_allocation(Z);
			for (int i = 0; i<q.n; i++)
				for (int j = 0; j<q.m; j++) {
					Z.A[i + 1][j] = q.A[i][j];
				}
			memory_cleaning(q);
			return Z;
		}}
	case'l': {if ((q.n == 0) || (q.m == 0)) {
		matrix Z;
		Z.n = Z.m = 1;
		Z = memory_allocation(Z);
		memory_cleaning(q);
		return Z;
	}
			 else {
				 matrix Z; Z.n = q.n; Z.m = q.m + 1;
				 Z = memory_allocation(Z);
				 for (int i = 0; i<q.n; i++)
					 for (int j = 0; j<q.m; j++) {
						 Z.A[i][j] = q.A[i][j];
					 }
				 memory_cleaning(q);
				 return Z;
			 }
	}
	case'j': {
		if ((q.n == 0) || (q.m == 0)) {
			matrix Z;
			Z.n = Z.m = 1;
			Z = memory_allocation(Z);
			memory_cleaning(q);
			return Z;
		}
		else {
			matrix Z; Z.n = q.n + 1; Z.m = q.m;
			Z = memory_allocation(Z);
			for (int i = 0; i<q.n; i++)
				for (int j = 0; j<q.m; j++) {
					Z.A[i][j] = q.A[i][j];
				}
			memory_cleaning(q);
			return Z;
		}
	}

	default: {
		cout << "символ не распознан\n";
		return q;
	}
	}
}

void search(matrix a, int k) {
	bool err = true;
	int dot = 0;
	for (int i = 0; i<a.n; i++)
		for (int j = 0; j < a.m; j++) {
			if (a.A[i][j] == k) {
				dot++;
			}
		}
	dot--;
	for (int i = 0; i<a.n; i++)
		for (int j = 0; j < a.m; j++) {
			if (a.A[i][j] == k) {
				cout << "[" << i << "]" << "[" << j << "]";
				if (dot) {
					cout << ", ";
					dot--;
				}
				err = false;
			}
		}
	if (err) {
		cout << "Элемент не найден" << endl;
		return;
	}

}

void modification(matrix a) {
	cout << "Введите позицию ([i][j]):";
	string number0;
	int  number1, number2;
	cin >> number0;
	number1 = atoi(number0.c_str() + 1);
	number2 = atoi(number0.c_str() + number0.rfind('[') + 1);

	if ((number1 < 0) || (number2 < 0) || (number1 >(a.n - 1)) || (number2 >(a.m - 1))) {
		cout << "Некорректная позиция!\n";
		return;
	}
	else {
		cout << "Введите новое значение:";
		int u;
		cin >> u;
		while (!cin) {
			cout << "ошибка, некоректный ввод!!\n";
			cin.clear();
			cin.get();
		}
		a.A[number1][number2] = u;
	}
}

matrix power(matrix a) {
	float pow = 0;
	int p = 0;
	cin >> pow;
	p = pow;
	if (pow - p != 0) {
		cout << "Cтепень должна быть целым числом! " << endl;
		return a;
	}

	if (p < 0) {
		cout << "Степень не должна быть отрицательной!" << endl;
		return a;
	}

	if (p == 1) {
		return a;
	}
	else if ((a.n != a.m) && (p != 1)) {
		cout << "Матрица не квадратична!" << endl;
		return a;
	}

	matrix C = memory_allocation(a);
	for (int z = 1; z < p; z++) {
		for (int i = 0; i < a.n; ++i) {
			for (int j = 0; j < a.m; ++j) {
				for (int k = 0; k < a.n; k++) {
					C.A[i][j] += a.A[i][k] * a.A[k][j];
				}
			}
		}
	}

	memory_cleaning(a);
	return C;
}

matrix polynomial(matrix a) {
	cout << "вычесление многочленна вида: aА^(n)+bA^(n-1)+cA^(n-2)+...+iA где a,b,c,...i принадлежит Z " << endl;
	if (a.n != a.m) {
		cout << "Матрица не квадратична!" << endl;
		return a;
	}
	cout << "введите количество членов матричного многочленна:" << endl;
	float ko = 0;
	int q = 0;
	cin >> ko;
	q = ko;
	if (ko - q != 0) {
		cout << "Количество членов должна быть целым числом! " << endl;
		return a;
	}
	if (q < 0) {
		cout << "Количество не должна быть отрицательным!" << endl;
		return a;
	}
	int cons;

	matrix k = memory_allocation(a);
	matrix sum = memory_allocation(a);
	matrix x = memory_allocation(a);

	for (int r = q; r > 0; r--) {
		cout << "Введите коэфициент перед A в степени " << r << ": ";
		cin >> cons;
		for (int i = 0; i < a.n; ++i) {
			for (int j = 0; j < a.m; ++j) {
				x.A[i][j] = a.A[i][j];
			}
		}
		if (r != 1) {

			for (int po = 1; po < q; po++) {
				for (int i = 0; i < a.n; ++i) {
					for (int j = 0; j < a.m; ++j) {
						for (int l = 0; l < a.n; l++) {
							k.A[i][j] += a.A[i][l] * x.A[l][j];
						}
					}
				}
				for (int i = 0; i < a.n; ++i) {
					for (int j = 0; j < a.m; ++j) {
						x.A[i][j] = k.A[i][j];
						k.A[i][j] = 0;
					}
				}
			}
		}
		for (int i = 0; i < a.n; ++i) {
			for (int j = 0; j < a.m; ++j) {
				x.A[i][j] *= cons;
			}
		}
		for (int i = 0; i < a.n; ++i) {
			for (int j = 0; j < a.m; ++j) {
				sum.A[i][j] += x.A[i][j];

			}
		}
	}
	memory_cleaning(a);
	memory_cleaning(k);
	memory_cleaning(x);
	return sum;
}


void det(matrix a) {
	if (a.n != a.m) {
		cout << "матрица не квадратная, определитель не существует !!" << endl;
		return;
	}
	if ((a.n == a.m) && (a.n == 1)) {
		cout << "det Matrix=" << a.A[0][0] << endl;
		return;
	}
	if ((a.n == a.m) && (a.n == 2)) {
		int s = 1, d = -1;
		for (int i = 0; i<a.n; i++)
			for (int j = 0; j < a.m; j++) {
				if (i == j) {
					s *= a.A[i][j];
				}
				if (i != j) {
					d *= a.A[i][j];
				}
			}
		cout << "det Matrix = " << s + d << endl;
		return;
	}
	if (a.n > 2) {
		int det;
		det = det_matrix_rec(a.A, a.m);
		cout << "det Matrix = " << det << endl;
	}

}

void algebraic_complements(int **mas, int **p, int i, int j, int m) {   // матрица м без i j строк/столбцов, возращает ту же сдвинутую к лев краю матрицу
	int di = 0, dj = 0;
	for (int ki = 0; ki<m - 1; ki++) {
		if (ki == i) di = 1;
		dj = 0;
		for (int kj = 0; kj<m - 1; kj++) {    
			if (kj == j) dj = 1;               
			p[ki][kj] = mas[ki + di][kj + dj];
		}
	}
}


int det_matrix_rec(int **a, int m) {
	int i, d, k, n;
	int **p = new int*[m];
	for (int i = 0; i < m; i++)
		p[i] = new int[m];

	d = 0;
	k = 1;
	n = m - 1;
	if (m < 1) cout << "Определитель вычислить невозможно!";
	if (m == 1) {
		d = a[0][0];
		for (int i = 0; i < m; i++)
			delete[] p[i];
		delete[] p;
		;
		return d;
	}
	if (m == 2) {
		d = a[0][0] * a[1][1] - (a[1][0] * a[0][1]);
		for (int i = 0; i < m; i++)
			delete[] p[i];
		delete[] p;
		return d;
	}
	if (m > 2) {
		for (i = 0; i < m; i++) {
			algebraic_complements(a, p, i, 0, m);
			d = d + k * a[i][0] * det_matrix_rec(p, n);
			k = -k;
		}
	}
	for (int i = 0; i < m; i++)
		delete[] p[i];
	delete[] p;
	return d;
}


void reverse(matrix a) {
	double det = det_matrix_rec(a.A, a.m);
	if (det == 0) {
		cout << "Т.к.определитель матрицы = 0, \nто матрица вырожденная и обратной не имеет" << endl;
		return;
	}
	if (det != 0) {
		double**Mat = new double*[a.n];
		double**reverse_t = new double*[a.n];
		for (int i = 0; i < a.n; i++) {
			reverse_t[i] = new double[a.n];
			Mat[i] = new double[a.n];
		}
		if (det) {
			for (int i = 0; i < a.n; i++) {
				for (int j = 0; j < a.n; j++) {
					int m = a.n - 1;
					int **temp_matr = new int *[m];
					for (int k = 0; k < m; k++)
						temp_matr[k] = new int[m];
					algebraic_complements(a.A, temp_matr, i, j, a.n);
					Mat[i][j] = pow(-1.0, i + j) * det_matrix_rec(temp_matr, m) / det;
					for (int i = 0; i < m; i++)
						delete[] temp_matr[i];
					delete[] temp_matr;
				}
			}
		}
		for (int i = 0; i<a.n; i++)
			for (int j = 0; j<a.n; j++) {

				reverse_t[i][j] = Mat[j][i];
			}
		for (int i = 0; i < a.n; ++i) {
			for (int j = 0; j < a.n; ++j) {
				cout << reverse_t[i][j] << "  ";
			}
			cout << endl;
		}
		for (int i = 0; i < a.n; i++)
			delete[] reverse_t[i];
		delete[] reverse_t;
		for (int i = 0; i < a.n; i++)
			delete[] Mat[i];
		delete[] Mat;
	}
}

void rec(matrix a)
{
	string filename;
	cout << " Укажите путь к файлу : ";
	cin >> filename;
	ifstream fin(filename.c_str());
	if (!fin.is_open()) {

		ofstream fout(filename.c_str());
		for (int i = 0; i < a.n; ++i) {
			for (int j = 0; j < a.m; ++j) {
				fout.width(5);
				fout << a.A[i][j] << " ";
			}
			fout << endl;
		}
		fout.close();
	}
	else {
		string answer;
		cout << "Перезаписать файл? (y, N)" << endl;
		cin >> answer;

		if ((answer == "y") || (answer == "Y") || (answer == "YES") || (answer == "yes") || (answer == "Yes")) {
			ofstream fout(filename.c_str());
			for (int i = 0; i < a.n; ++i) {
				for (int j = 0; j < a.m; ++j) {
					fout.width(5);
					fout << a.A[i][j] << " ";
				}
				fout << endl;
			}
			fout.close();
		}
		else {
			return;
		}

	}
}

void  downloading(matrix a)
{

}

